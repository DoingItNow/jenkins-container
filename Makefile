RUNNING_CONTAINER_TO_DELETE=$(shell docker ps -a --format {{.Names}} | grep local_jenkins)
RUNNING_CONTAINER_TO_DELETE_COMPOSE=$(shell docker ps -a --format {{.Names}} | grep stpjenkinscontainer_jenkins_1)
STOPPED_CONTAINER_TO_DELETE=$(shell docker ps -qa --no-trunc --filter "status=exited")
COOL_NAME=$(shell curl https://frightanic.com/goodies_content/docker-names.php)


build:
	docker build -t jenkins-container -f Dockerfile .

run:
	docker run  --name local_jenkins -d -p 8080:8080 -p 50000:50000 -v local_jenkins_home:/var/jenkins_home jenkins-container
	docker exec local_jenkins cat /var/jenkins_home/secrets/initialAdminPassword