# Jenkins in Docker

## What does this repo do
This is a reference resource to spin up a jenkins system in docker.

## How to add custom packages
Go to `ansible/main.xml `and add in any ansible roles you desire. Once you add the roles, add in the git repos into `requirement.yml`. A sample of packages are given already.

## Creating jenkins container
* Run the following command `make initial_build`
* Go to `localhost:8080`
  * NOTE: This may be on another IP address thats not localhost
* Log into Jenkins with `admin` as your username and `password` as your password

## Creating more containers
Once the docker image is built you can just run `make run` to start up a new container.

## Cleaning up docker containers/images
There are two forms of cleaning up container or images. To run them use `make`
* `make stop` :  stops the container.
* `make reset` : removes jenkins container and the corrosponding volume attach to it.
* `make hard_clean` : removes jenkins containers and the corrosponding volumes attach to it. It will also delete the image it was built with.

## Update plugins.txt for the repo
This is on applicable if you want to update the plugins.txt in the repo.
* Go to `Manage Jenkins`  --> `Script Console`
* In the script console, type in the following code 
  ```
  Jenkins.instance.pluginManager.plugins.each {
    plugin -> 
      println ("${plugin.getShortName()}:${plugin.getVersion()}")
  }
  ```
* Copy and paste the contents into plugins.txt in your repo
* Type in the following command 
  
  `sort plugins.txt > plugins2.txt && rm -rf plugins.txt && mv plugins2.txt plugins.txt`

* Push those changes into a branch (or master)